#!/bin/sh

# jump to script directory if not called from local folder
cd "$(dirname "$(readlink -f "$0")")"

# RUN script for FCS Aggregator

# rebuild
docker build --tag=textplus-fcs-aggregator .
if [ $? -ne 0 ]; then
    exit 1
fi

# stop old
OLDID=$(docker ps | grep 0.0.0.0:4019 | cut -d' ' -f1)
if [ ! -z "$OLDID" ]; then
    docker stop $OLDID
    docker rm $OLDID
fi

# run
if [ ! -f fcsAggregatorResources.json ]; then
    touch fcsAggregatorResources.json fcsAggregatorResources.backup.json
fi
docker run \
    -d \
    --restart unless-stopped \
    -p 4019:4019 \
    -p 5005:5005 \
    -v $(pwd)/aggregator.yml:/work/aggregator.yml:ro \
    -v $(pwd)/fcsAggregatorResources.json:/var/lib/aggregator/fcsAggregatorResources.json \
    -v $(pwd)/fcsAggregatorResources.backup.json:/var/lib/aggregator/fcsAggregatorResources.backup.json \
    --name textplus-fcs-aggregator \
    textplus-fcs-aggregator
