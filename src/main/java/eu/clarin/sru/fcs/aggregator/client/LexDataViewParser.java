package eu.clarin.sru.fcs.aggregator.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.clarin.sru.client.SRUClientException;
import eu.clarin.sru.client.XmlStreamReaderUtils;
import eu.clarin.sru.client.fcs.DataView;
import eu.clarin.sru.client.fcs.DataViewParser;

public class LexDataViewParser implements DataViewParser {

    private static final String FCS_LEX_NS = "http://clarin.eu/fcs/dataview/lex";
    private static final Logger logger = LoggerFactory.getLogger(LexDataViewParser.class);

    @Override
    public boolean acceptType(String type) {
        return DataViewLex.TYPE.equals(type);
    }

    @Override
    public int getPriority() {
        return 1000;
    }

    @Override
    public DataView parse(XMLStreamReader reader, String type, String pid, String ref)
            throws XMLStreamException, SRUClientException {
        XmlStreamReaderUtils.readStart(reader, FCS_LEX_NS, "Entry", true, true);
        final String entryXmlLang = XmlStreamReaderUtils.readAttributeValue(reader, XMLConstants.XML_NS_URI, "lang");
        final String entryLangUri = XmlStreamReaderUtils.readAttributeValue(reader, null, "langUri");
        reader.next(); // skip start element

        logger.debug("entry: xml:lang={}, langUri={}", entryXmlLang, entryLangUri);

        final List<DataViewLex.Field> fields = new ArrayList<>();
        while (XmlStreamReaderUtils.readStart(reader, FCS_LEX_NS, "Field", fields.isEmpty(), true)) {
            final String fieldTypeRaw = XmlStreamReaderUtils.readAttributeValue(reader, null, "type");
            final DataViewLex.FieldType fieldType = DataViewLex.FieldType.fromString(fieldTypeRaw);
            reader.next(); // skip start element

            logger.debug("field: type={}", fieldType);

            final List<DataViewLex.Value> values = new ArrayList<>();
            while (XmlStreamReaderUtils.readStart(reader, FCS_LEX_NS, "Value", values.isEmpty(), true)) {
                final String xmlLang = XmlStreamReaderUtils.readAttributeValue(reader, XMLConstants.XML_NS_URI, "lang");
                final String xmlId = XmlStreamReaderUtils.readAttributeValue(reader, XMLConstants.XML_NS_URI, "id");

                final String langUri = XmlStreamReaderUtils.readAttributeValue(reader, null, "langUri");
                final boolean preferred = Boolean
                        .parseBoolean(XmlStreamReaderUtils.readAttributeValue(reader, null, "preferred"));
                final String refRaw = XmlStreamReaderUtils.readAttributeValue(reader, null, "ref");
                final String idrefsRaw = XmlStreamReaderUtils.readAttributeValue(reader, null, "idrefs");
                List<String> idrefs = null;
                if (idrefsRaw != null) {
                    idrefs = Arrays.asList(idrefsRaw.trim().replaceAll("\\s+", " ").split(" "));
                }
                final String vocabRef = XmlStreamReaderUtils.readAttributeValue(reader, null, "vocabRef");
                final String vocabValueRef = XmlStreamReaderUtils.readAttributeValue(reader, null, "vocabValueRef");
                final String typeRaw = XmlStreamReaderUtils.readAttributeValue(reader, null, "type");
                final String source = XmlStreamReaderUtils.readAttributeValue(reader, null, "source");
                final String sourceRef = XmlStreamReaderUtils.readAttributeValue(reader, null, "sourceRef");
                final String date = XmlStreamReaderUtils.readAttributeValue(reader, null, "date");
                final String relRaw = XmlStreamReaderUtils.readAttributeValue(reader, null, "rel");
                final DataViewLex.FieldType rel = (relRaw != null) ? DataViewLex.FieldType.fromString(relRaw) : null;

                reader.next(); // skip start element

                String content = XmlStreamReaderUtils.readString(reader, false);
                XmlStreamReaderUtils.readEnd(reader, FCS_LEX_NS, "Value");

                logger.debug("value: content='{}', xml:id={}, xml:lang={}, langUri={}, "
                        + "preferred={}, ref={}, idrefs={}, vocabRef={}, vocabValueRef={}, type={}"
                        + "source={}, sourceRef={}, date={}, rel={}",
                        content, xmlId, xmlLang, langUri, preferred, refRaw, idrefs, vocabRef, vocabValueRef,
                        typeRaw, source, sourceRef, date, rel);
                DataViewLex.Value value = new DataViewLex.Value(content, xmlId, xmlLang, langUri, preferred,
                        refRaw, idrefs, vocabRef, vocabValueRef, typeRaw, source, sourceRef, date, rel);
                values.add(value);
            }

            XmlStreamReaderUtils.readEnd(reader, FCS_LEX_NS, "Field");

            DataViewLex.Field field = new DataViewLex.Field(fieldType, values);
            fields.add(field);
        } // while

        XmlStreamReaderUtils.readEnd(reader, FCS_LEX_NS, "Entry");

        // TODO: check id/idrefs valid
        // throw new SRUClientException("missing id target for idrefs")

        return new DataViewLex(pid, ref, fields, entryXmlLang, entryLangUri);
    }
}
