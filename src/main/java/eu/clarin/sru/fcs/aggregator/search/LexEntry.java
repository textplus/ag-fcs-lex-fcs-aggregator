package eu.clarin.sru.fcs.aggregator.search;

import java.util.List;

import eu.clarin.sru.fcs.aggregator.client.DataViewLex;

public class LexEntry {

    private List<DataViewLex.Field> fields;
    private String pid;
    private String reference;

    public LexEntry(DataViewLex hits, String pid, String reference) {
        this.fields = hits.getFields();
        this.pid = pid;
        this.reference = reference;
    }

    public List<DataViewLex.Field> getFields() {
        return fields;
    }

    public String getPid() {
        return pid;
    }

    public String getReference() {
        return reference;
    }

}
