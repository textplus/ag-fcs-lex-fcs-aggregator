package eu.clarin.sru.fcs.aggregator.client;

import eu.clarin.sru.client.fcs.DataViewHits;

public class DataViewLexHits extends DataViewHits {

    private final String[] hitKinds;
    public static final String TYPE = "application/x-clarin-fcs-hits+xml";
    private final String text;
    private final int[] offsets;
    private final int max_offset;

    protected DataViewLexHits(String pid, String ref, String text,
            int[] offsets, int offsets_idx, String[] hitKinds) {
        super(pid, ref, text, offsets, offsets_idx);
        if (text == null) {
            throw new NullPointerException("text == null");
        }
        if (text.isEmpty()) {
            throw new IllegalArgumentException("text is empty");
        }
        this.text = text;
        if (offsets == null) {
            throw new NullPointerException("offsets == null");
        }
        this.offsets = offsets;
        if (offsets_idx < 0) {
            throw new IllegalArgumentException("offset_idx < 0");
        }
        if (offsets_idx > offsets.length) {
            throw new IllegalArgumentException("offset_idx > offsets.length");
        }
        this.max_offset = (offsets_idx / 2);
        if (hitKinds == null) {
            throw new NullPointerException("hitKinds == null");
        }
        if ((offsets_idx / 2) > hitKinds.length) {
            throw new IllegalArgumentException("(offsets_idx / 2) > hitKinds.length");
        }
        this.hitKinds = hitKinds;
    }

    /**
     * Get the total number of hits in the result.
     * 
     * @return the number of hits
     */
    public int getHitCount() {
        return max_offset;
    }

    /**
     * Get the text content of the hit. Usually this is complete sentence.
     *
     * @return the text content of the hit
     */
    public String getText() {
        return text;
    }

    /**
     * Get the offsets pointing to range in the text content that yield the hit.
     *
     * @param idx
     *            the hit to retrieve. Must be larger than <code>0</code> and
     *            smaller than the result of {@link #getHitCount()}.
     * @return An array of two elements. The first array element is the start
     *         offset, the second array element is the end offset of the hit
     *         range.
     * @throws ArrayIndexOutOfBoundsException
     *                                        of the <code>idx</code> argument is
     *                                        out of bounds.
     */
    public int[] getHitOffsets(int idx) {
        if (idx < 0) {
            throw new ArrayIndexOutOfBoundsException("idx < 0");
        }
        if (idx < max_offset) {
            int[] result = new int[2];
            result[0] = offsets[(2 * idx)];
            result[1] = offsets[(2 * idx) + 1];
            return result;
        } else {
            throw new ArrayIndexOutOfBoundsException("idx > " +
                    (max_offset - 1));
        }
    }

    public String getHitKind(int idx) {
        if (idx < 0) {
            throw new ArrayIndexOutOfBoundsException("idx < 0");
        }
        if (idx < max_offset) {
            return hitKinds[idx];
        } else {
            throw new ArrayIndexOutOfBoundsException("idx > " +
                    (max_offset - 1));
        }
    }
}
