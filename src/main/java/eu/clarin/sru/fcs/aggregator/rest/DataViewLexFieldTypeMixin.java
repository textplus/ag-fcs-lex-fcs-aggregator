package eu.clarin.sru.fcs.aggregator.rest;

import com.fasterxml.jackson.annotation.JsonValue;

public abstract class DataViewLexFieldTypeMixin {
    @JsonValue
    abstract String getType();
}
